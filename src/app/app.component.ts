import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  countries = [
    { id: 1, name: "España" },
    { id: 2, name: "Italia" },
    { id: 3, name: "Francia" }
  ];

  form: any;
  submitted = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      acceptTerms: [false, Validators.requiredTrue],
      emailInput: ["", [Validators.required, Validators.email]],
      countryInput: ["", Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    alert('¡Perfecto, te avisaremos la primera!');
  }
}
